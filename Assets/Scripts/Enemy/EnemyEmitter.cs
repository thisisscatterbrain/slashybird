﻿using UnityEngine;
using System.Collections;

public class EnemyEmitter : MonoBehaviour {

	public float m_emitDelta = 1.0f;
	public float m_emitElapsed = 0.0f;
	private GlobalClock m_globalClock;
	private Object m_prefab;
	private Bounds m_bounds;

	void Awake() 
	{
		m_globalClock = GameObject.Find("GlobalClock").GetComponent<GlobalClock>();
		m_prefab = Resources.Load("Enemy");
		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = Camera.main.orthographicSize * 2;
		m_bounds = new Bounds(Camera.main.transform.position, new Vector3(screenAspect * cameraHeight, cameraHeight, 0.0f));
	}
	
	void Update()
	{
		if (m_emitElapsed > m_emitDelta) {
			float x = m_bounds.center.x + m_bounds.extents.x;
			float y = m_bounds.center.y + Random.Range(-m_bounds.extents.y, m_bounds.extents.y);
			m_emitElapsed = 0.0f;
			Instantiate(m_prefab, new Vector3(x, y, 0.0f), Quaternion.identity);
		}

		m_emitElapsed += m_globalClock.m_timeCoeff * Time.deltaTime;
	}
}
