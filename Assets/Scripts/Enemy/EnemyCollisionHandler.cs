﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionHandler : MonoBehaviour {

	private PlayerMotion m_playerMotion;
	private Animator m_animator;


	void Awake()
	{
		GameObject slashy = GameObject.Find ("SlashyBird");

		if (!slashy) {
			return;
		}

		this.m_playerMotion = slashy.GetComponent<PlayerMotion> ();
	}

	void Start () {
		m_animator = GetComponent<Animator>();
	}

	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (!m_playerMotion) {
			return;
		}

		// Ignore collisions with other enemies.
		if (other.name != "SlashyBird") {
			return;
		}
	
		if (m_playerMotion.GetState () == PlayerMotion.State.MOVE_SLASH) {
			m_animator.Play("Explosion");
			this.transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
		}
	}

	void Die()
	{
		GameObject.Destroy (this.gameObject);
	}
}
