﻿using UnityEngine;
using System.Collections;

public class LineProjectileEmitter : MonoBehaviour {
	private Object m_projectilePrefab;
	public float m_emissionTime = 2.0f;
	public float m_elapsed = 0.0f;
	private EnemyMotion m_motion;
	
	void Start() {
		m_projectilePrefab = Resources.Load("Projectile");
		m_motion = this.GetComponent<EnemyMotion>();
	}

	void Update () {
		m_elapsed += Time.deltaTime;

		if (m_elapsed > m_emissionTime) {
			m_elapsed = 0.0f;
			EmitProjectile();
		}
	}

	void EmitProjectile()
	{
		GameObject o = (GameObject)Instantiate(m_projectilePrefab, new Vector3(this.transform.position.x, this.transform.position.y, 0.0f), Quaternion.identity);
		o.GetComponent<ProjectileMotion>().m_direction = m_motion.m_direction; 
	}
}
