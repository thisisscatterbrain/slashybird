﻿using UnityEngine;
using System.Collections;

public class EnemyMotion : MonoBehaviour {

	public float m_speed = 1.0f;
	public Vector3 m_direction = new Vector3(-1.0f, 0.0f, 0.0f);
	private GlobalClock m_globalClock;
	
	void Start() 
	{
		m_globalClock = GameObject.Find("GlobalClock").GetComponent<GlobalClock>();
	}

	void Update() 
	{
		this.transform.position += m_globalClock.m_timeCoeff * m_speed * Time.deltaTime * (new Vector3(-1.0f, 0.0f, 0.0f));
	}
}
