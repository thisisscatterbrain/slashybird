﻿using UnityEngine;
using System.Collections;

public class ProjectileMotion : MonoBehaviour {

	public float m_speed = .5f;
	public Vector3 m_direction = new Vector3(1.0f, 0.0f, 0.0f);
	private GlobalClock m_globalClock;

	// Use this for initialization
	void Start () {
		m_globalClock = GameObject.Find("GlobalClock").GetComponent<GlobalClock>();
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position += m_speed * m_direction * Time.deltaTime * m_globalClock.m_timeCoeff; 
	}
}
