﻿using UnityEngine;
using System.Collections;

public class GlobalClock : MonoBehaviour {
	// Should always be the coefficient for [Time.deltaTime].
	public float m_timeCoeff = 1.0f;
}
