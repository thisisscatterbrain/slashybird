﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMotion : MonoBehaviour {

	public enum State {
		MOVE_NORMAL = 0,
		MOVE_RECORD_MOTION,
		MOVE_SLASH,
		MOVE_PAUSE
	};

	private Animator m_animator;

	private const float EPS = 0.01f;
	private State m_state;
	public Camera m_camera;
	public float m_speed = 1.0f;
	public float m_slashThresh = 4.0f;

	// Time the player spends slashing.
	public float m_slashTime = 1.0f;
	public float m_slashSpeed = 5.0f;
	private float m_slashTimeElapsed;
	public float m_pauseTime = 1.0f;
	public float m_pauseTimeElapsed;
	private Vector3 m_screenPosMin;
	private Vector3 m_screenPosMax;	
	private Vector3 m_dAxis = new Vector3(0.0f, 0.0f, 0.0f);
	private Queue m_slashSamples;
	private GlobalClock m_globalClock;

	// This variable dictates how long the play can "plan" his/her slash.
	public float m_recordingInterval = 2.0f;
	public float m_recordingElapsed = 0.0f;
	

	void Start() {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		m_state = State.MOVE_NORMAL;
		m_screenPosMin = m_camera.ScreenToWorldPoint(new Vector3 (0.0f, 0.0f, 0.0f));
		m_screenPosMax = m_camera.ScreenToWorldPoint(new Vector3 (Screen.width, Screen.height, 0.0f));
		m_slashSamples = new Queue ();
		m_globalClock = GameObject.Find("GlobalClock").GetComponent<GlobalClock>();
		m_animator = GetComponent<Animator>();
	}
	
	void Update() {
		if (m_state == State.MOVE_NORMAL) {
			m_state = UpdateNormal ();
		} else if (m_state == State.MOVE_SLASH) {
			m_state = UpdateSlash ();
		} else if (m_state == State.MOVE_PAUSE) {
			m_state = UpdatePause ();
		} else if (m_state == State.MOVE_RECORD_MOTION) {
			m_state = UpdateRecordMotion ();
		}
	}

	State UpdateNormal() {
		m_dAxis = new Vector3(Input.GetAxis ("Mouse X"), Input.GetAxis ("Mouse Y"), 0.0f);
		this.transform.position = this.transform.position + Time.deltaTime * m_speed * m_dAxis;

		if (Input.GetMouseButtonDown(0)) {
			m_globalClock.m_timeCoeff = 0.0f;
			m_recordingElapsed = 0.0f;
			m_animator.Play("Slashing");
			//m_animator.SetTrigger("Slash");
			return State.MOVE_RECORD_MOTION;
		}

		ClampPosition();
		return State.MOVE_NORMAL;
	}

	State UpdateSlash() {
		if (m_slashSamples.Count == 0) {
			m_animator.Play("PutSwordBack");
			return State.MOVE_NORMAL;
		}

		Vector3 p = (Vector3)m_slashSamples.Dequeue ();
		this.transform.position = this.transform.position + Time.deltaTime * m_slashSpeed * p;

		if (p.x < 0) {
			this.transform.localScale = new Vector3 (-Math.Abs (this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);
		} else {
			this.transform.localScale = new Vector3(Math.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);
		}
		return State.MOVE_SLASH;
	}

	State UpdateRecordMotion()
	{
		float dx = Input.GetAxis("Mouse X");
		float dy = Input.GetAxis("Mouse Y");

		if (!((Math.Abs(dx) < EPS) &&(Math.Abs(dy) < EPS))) {
			m_slashSamples.Enqueue(new Vector3(dx, dy, 0.0f));
		}

		if (Input.GetMouseButtonUp(0) || (m_recordingElapsed > m_recordingInterval)) {
			m_globalClock.m_timeCoeff = 1.0f;
			return State.MOVE_SLASH;
		}

		m_recordingElapsed += Time.deltaTime;
		return State.MOVE_RECORD_MOTION;
	}

	State UpdatePause() {
		if (m_pauseTimeElapsed > m_pauseTime) {
			return State.MOVE_NORMAL;
		}

		m_pauseTimeElapsed += Time.deltaTime;
		return State.MOVE_PAUSE;
	}

	void ClampPosition () {
		Vector3 v = new Vector3 (0.0f, 0.0f, 0.0f);
		v.x = Mathf.Clamp(this.transform.position.x, m_screenPosMin.x, m_screenPosMax.x);
		v.y = Mathf.Clamp(this.transform.position.y, m_screenPosMin.y, m_screenPosMax.y);
		this.transform.position = v;
	}

	public State GetState ()
	{
		return m_state;
	}
}
