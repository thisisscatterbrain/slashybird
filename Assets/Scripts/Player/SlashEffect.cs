﻿using UnityEngine;
using System.Collections;

public class SlashEffect : MonoBehaviour {
	
	public LineRenderer m_slashRenderer;

	private PlayerMotion m_playerMotion;

	//slash rendering
	private int m_slashVertices = 0;

	public float m_fadeTime;
	private float m_fadeElapsed;

	private Color m_endColor = new Color(1f,0f,0f,1f);
	private Color m_startColor = new Color(1f,0f,0f,0f);


	private PlayerMotion.State m_previousState = PlayerMotion.State.MOVE_NORMAL;

	// Use this for initialization
	void Start () {
		m_playerMotion = GetComponent<PlayerMotion> ();
	}
	
	// Update is called once per frame
	void Update () {
		PlayerMotion.State state = m_playerMotion.GetState ();
		if (state == PlayerMotion.State.MOVE_SLASH) {
			if (m_previousState != PlayerMotion.State.MOVE_SLASH) {
				StartSlash ();
			} else {
				UpdateSlash (m_playerMotion.transform.position);
			}
		} else {
			m_fadeElapsed -= Time.deltaTime;
			FadeSlash();
		}

		m_previousState = state;
	}

	void StartSlash(){
		//slash render
		m_slashVertices = 0;
		m_slashRenderer.SetVertexCount (0);
		m_fadeElapsed = m_fadeTime;
		
		m_slashRenderer.SetColors (m_startColor, m_endColor);

	}

	void UpdateSlash(Vector3 pos){	
		//slash effect
		Vector3 behindPos = new Vector3 (pos.x, pos.y, pos.z + 0.1f);
		++m_slashVertices;
		m_slashRenderer.SetVertexCount (m_slashVertices);
		m_slashRenderer.SetPosition (m_slashVertices - 1, behindPos);
	}

	void FadeSlash(){
		//m_slashRenderer.SetVertexCount (0);

		Color color = new Color (m_endColor.r, m_endColor.g, m_endColor.b, m_fadeElapsed / m_fadeTime);

		m_slashRenderer.SetColors (m_startColor, color);
	}
}
