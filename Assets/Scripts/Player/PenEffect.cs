﻿using UnityEngine;
using System.Collections;

public class PenEffect : MonoBehaviour {

	public LineRenderer m_penRenderer;
	public PlayerMotion m_player;

	private int m_penVertices = 0;

	private float m_prevDx, m_prevDy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if(m_player.GetState() == PlayerMotion.State.MOVE_RECORD_MOTION){

			float dx = Input.GetAxis("Mouse X") * Time.deltaTime * m_player.m_slashSpeed;
			float dy = Input.GetAxis("Mouse Y") * Time.deltaTime * m_player.m_slashSpeed;

			++m_penVertices;
			m_penRenderer.SetVertexCount(m_penVertices);
			m_penRenderer.SetPosition(m_penVertices-1, new Vector3(m_prevDx + dx, m_prevDy + dy, m_player.transform.position.z+1));

			m_prevDx += dx;
			m_prevDy += dy;

		}else{
			m_penVertices = 0;
			m_penRenderer.SetVertexCount(0);

			m_prevDx = m_player.transform.position.x;
			m_prevDy = m_player.transform.position.y;
		}
	}
}
